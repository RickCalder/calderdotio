'use strict';

// Include Gulp and plugins
var gulp = require('gulp');
var $    = require('gulp-load-plugins')();

// Include additional tools
var runSequence = require('run-sequence');

gulp.task('images:compress', function() {
  return gulp.src(['assets/vendor/img/**/*', 'assets/src/img/**/*'])
    .pipe($.imagemin({
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest('assets/dist/img'))
    .pipe($.size({title: 'images:compress'}));
});

gulp.task('images:svg', function() {
  return gulp.src(['assets/src/svg/**/*'])
    .pipe($.imagemin({
      multipass: true,
      svgoPlugins: [
        { removeViewBox: true },
        { removeUselessStrokeAndFill: true },
        { removeEmptyAttrs: true }
      ]
    }))
    .pipe(gulp.dest('assets/dist/svg'));
});

gulp.task('images:svg2png', function() {
  return gulp.src(['assets/dist/svg/**/*'])
    .pipe($.svg2png())
    .pipe(gulp.dest('assets/dist/img'));
});

gulp.task('images', function(next) {
  return runSequence(['images:svg', 'images:compress'], next);
});

gulp.task('styles:compile', function() {
  return gulp.src(['assets/vendor/scss/**/*.scss', 'assets/src/scss/**/*.scss'])
    .pipe($.sass())
    .pipe(gulp.dest('assets/src/css'))
    .pipe($.size({title: 'styles:compile'}));
});

gulp.task('styles:compress', function() {
  return gulp.src(['assets/vendor/css/**/*.css', 'assets/src/css/**/*.css'])
    .pipe($.concatCss('main.css'))
    .pipe($.autoprefixer('last 1 versions'))
    .pipe($.rename('main.min.css'))
    .pipe($.csso())
    .pipe(gulp.dest('assets/dist/css'))
    .pipe($.size({title: 'styles:compress'}));
});

gulp.task('styles', function(next) {
  return runSequence('styles:compile', 'styles:compress', 'images:compress', next);
});

gulp.task('scripts:compress', function() {
  return gulp.src([
      'assets/vendor/js/**/*.js',
      'assets/src/js/*.js'
    ])
    .pipe($.sourcemaps.init({loadMaps: true}))
    .pipe($.concat('main.min.js'))
    .pipe($.uglify())
    .pipe($.chmod(755))
    .pipe(gulp.dest('assets/dist/js'))
    .pipe($.size({title: 'scripts:compress'}));
});

gulp.task('scripts', function(next) {
  return runSequence('scripts:compress', next);
});

gulp.task('fonts:move', function() {
  return gulp.src('assets/src/fonts/**/*')
    .pipe(gulp.dest('assets/dist/fonts'));
});

gulp.task('fonts', function(next) {
  return runSequence('fonts:move', next);
});

/**
 * Run images
 * Run styles, scripts, and fonts in parallel
 * Run callback function
 */
gulp.task('build', function(next) {
  return runSequence('images', ['styles', 'scripts', 'fonts'], next);
});

gulp.task('default', function() {
  gulp.start('build', function() {
    gulp.watch('assets/vendor/js/**/*.js', ['scripts']);
    gulp.watch('assets/src/js/**/*.js', ['scripts']);
    gulp.watch('assets/src/js/admin/*.js', ['scripts']);
    gulp.watch('assets/src/js/user/*.js', ['scripts']);
    gulp.watch('assets/src/js/registration/*.js', ['scripts']);
    gulp.watch('assets/vendor/css/**/*.css', ['styles']);
    gulp.watch('assets/vendor/scss/**/*.scss', ['styles']);
    gulp.watch('assets/src/scss/**/*.scss', ['styles']);
    gulp.watch('assets/vendor/img/**/*', ['images']);
    gulp.watch('assets/src/img/**/*', ['images']);
    gulp.watch('assets/src/svg/**/*', ['images']);
    gulp.watch('assets/src/fonts/**/*', ['fonts']);

  });
});
