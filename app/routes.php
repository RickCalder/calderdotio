<?php

    // Creating routes

    // Psr-7 Request and Response interfaces
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Mailgun\Mailgun;

    
// HOME ROUTE //

$app->get('/', function (Request $request, Response $response, $args) {

  $posts = json_decode(file_get_contents('assets/dist/data/posts.json'));
  $post_count = count($posts);
  $testimonials = [
    [
    'client' => 'Andy Huggett, Execute Marketing',
    'testimonial' => '<p>Rick is one of those rare developers who cares about the project as much as the client does. He has an excellent breadth of development skills but also understands the commercial needs of a web project. He built our web site and web app from scratch, adding functionality and implementing improvements along the way. Rick was highly recommended to me and I have no hesitation in passing that high recommendation on to anyone reading this.</p>'
    ],
    [
    'client' => 'Andy Huggett, Execute Marketing',
    'testimonial' => '<p>Rick is one of those rare developers who cares about the project as much as the client does. He has an excellent breadth of development skills but also understands the commercial needs of a web project. He built our web site and web app from scratch, adding functionality and implementing improvements along the way. Rick was highly recommended to me and I have no hesitation in passing that high recommendation on to anyone reading this.</p>'
    ],
    [
    'client' => 'Bruno Venesia, Refficient Inc.',
    'testimonial' => '<p>Rick is a talented web developer with a strong flair for design and a great work ethic. He has the ability to listen to the wants and needs of his managers and co-workers, and turn them into reality in the finished product. He is attentive to detail and always looks for ways to improve the online experience for our customers and employees.</p>'
    ],
  ];

  $testimonial = array_rand($testimonials, 1);
  
  $vars = [
    'page' => [
      'title' => 'calder.io',
      'description' => 'Web development and support for small to medium sized business.',
      'site_image'  => 'https://calder.io/assets/dist/img/calder-header.jpg',
      'testimonial' => $testimonials[$testimonial],
      'posts'       => $posts,
      'post_count'  =>$post_count,
      'social_media' => [
        'facebook' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg>',
          'link' => 'https://www.facebook.com/calderdotio'
        ],
        'twitter' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/></svg>',
          'link' => 'https://twitter.com/calderdotio'
        ],
        'github' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M256 70.7c-102.6 0-185.9 83.2-185.9 185.9 0 82.1 53.3 151.8 127.1 176.4 9.3 1.7 12.3-4 12.3-8.9V389.4c-51.7 11.3-62.5-21.9-62.5-21.9 -8.4-21.5-20.6-27.2-20.6-27.2 -16.9-11.5 1.3-11.3 1.3-11.3 18.7 1.3 28.5 19.2 28.5 19.2 16.6 28.4 43.5 20.2 54.1 15.4 1.7-12 6.5-20.2 11.8-24.9 -41.3-4.7-84.7-20.6-84.7-91.9 0-20.3 7.3-36.9 19.2-49.9 -1.9-4.7-8.3-23.6 1.8-49.2 0 0 15.6-5 51.1 19.1 14.8-4.1 30.7-6.2 46.5-6.3 15.8 0.1 31.7 2.1 46.6 6.3 35.5-24 51.1-19.1 51.1-19.1 10.1 25.6 3.8 44.5 1.8 49.2 11.9 13 19.1 29.6 19.1 49.9 0 71.4-43.5 87.1-84.9 91.7 6.7 5.8 12.8 17.1 12.8 34.4 0 24.9 0 44.9 0 51 0 4.9 3 10.7 12.4 8.9 73.8-24.6 127-94.3 127-176.4C441.9 153.9 358.6 70.7 256 70.7z"/></svg>',
          'link' => 'https://github.com/calderdotio'
        ],
        'stackoverflow' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M294.8 361.2l-122 0.1 0-26 122-0.1L294.8 361.2zM377.2 213.7L356.4 93.5l-25.7 4.5 20.9 120.2L377.2 213.7zM297.8 301.8l-121.4-11.2 -2.4 25.9 121.4 11.2L297.8 301.8zM305.8 267.8l-117.8-31.7 -6.8 25.2 117.8 31.7L305.8 267.8zM321.2 238l-105-62 -13.2 22.4 105 62L321.2 238zM346.9 219.7l-68.7-100.8 -21.5 14.7 68.7 100.8L346.9 219.7zM315.5 275.5v106.5H155.6V275.5h-20.8v126.9h201.5V275.5H315.5z"/></svg>',
          'link' => 'http://stackoverflow.com/users/1726511/rick-calder'
        ],
        'linkedin' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/></svg>',
          'link' => 'https://www.linkedin.com/company/calder.io'
        ]
      ]
    ],
  ];  
  return $this->view->render($response, 'home.twig', $vars);

})->setName('home');

$app->post('/', function (Request $request, Response $response, $args) {
  $post = $request->getParsedBody();
  if($post['email'] == 'sample@email.tst' ){
    return;
  };
  if($post['fax'] != '' ){
    return;
  };

  $mg = new Mailgun('key-5548157eee42d7853370c749c26f177d');

  $text = 'From: ' . $post['full_name'] . '<br>Email: ' . $post['email'] . '<br>Company: '. $post['company'] . '<br>Website: ' . $post['website'] . '<br>Reason: ' . $post['reason'] . '<br>Message: ' . nl2br($post['comments']);

  # Now, compose and send your message.
  $response = $mg->sendMessage('mg.calder.io', [
    'from'    => $post['email'], 
    'to'      => 'calder12@gmail.com', 
    'cc'      => 'rick@calder.io',
    'subject' => 'Message from calder.io website', 
    'html'    => $text
  ]);

  if( $response->http_response_code == '200' ) {
    print json_encode( array('status'=> 'sent'));
  } else {
    print json_encode( array('status'=> 'error'));
  }
});

$app->post('/update_posts', function (Request $request, Response $response, $args) {
  if($request->getUri()->getBaseUrl() == 'http://calder.dev' ){
    $api_url = 'http://wp-calder.dev';
  } else {
    $api_url = 'http://wp.calderonline.com';
  }

  $client = new GuzzleHttp\Client();
  $data = $client->request('GET', $api_url . '/wp-json/wp/v2/posts')->getBody()->getContents();
  
  $posts = json_decode($data);
  $new_posts = array();
  $i = 0;
  foreach( $posts as $post ) {
    $new_posts[$i]['title'] = $post->title->rendered;
    $new_posts[$i]['excerpt'] = $post->excerpt->rendered;
    $new_posts[$i]['content'] = str_replace('/content/uploads/', $request->getUri()->getBaseUrl() . '/assets/dist/img/uploads/', $post->content->rendered);
    $new_posts[$i]['slug'] = $post->slug;

    //handle post images
    $images_counter = 0;
    $images = json_decode($client->request( 'GET', $api_url . '/wp-json/wp/v2/media?parent=' . $post->id)->getBody()->getContents() );
    foreach( $images as $image ) {
      foreach( $image->media_details->sizes as $current_image ) {
        $new_directory = str_replace($current_image->file, '', $current_image->source_url);
        $new_directory = str_replace('/content/uploads/', 'assets/dist/img/uploads/', $new_directory);
        //if the uploads folder doesn't exist create it
        if (!file_exists($new_directory)) {
          mkdir($new_directory, 0755, true);
        }

        //if we don't already have the image save it
        if( !file_exists($new_directory . $current_image->file) ){
          $image_to_save = file_get_contents($api_url . $current_image->source_url);
          file_put_contents($new_directory . $current_image->file, $image_to_save);
        }
      }
      $images_counter++;
    }
    $featured_small = json_decode($client->request('GET', $api_url . '/wp-json/wp/v2/media/' . $post->featured_media)->getBody()->getContents());
    if(isset($featured_small->media_details->sizes->{'twentyseventeen-featured-image'})) {
      $new_posts[$i]['featured_image'] = str_replace('/content/uploads/', $request->getUri()->getBaseUrl() . '/assets/dist/img/uploads/',$featured_small->media_details->sizes->{'twentyseventeen-featured-image'}->source_url);
    } else {
      $new_posts[$i]['featured_image'] = str_replace('/content/uploads/', $request->getUri()->getBaseUrl() . '/assets/dist/img/uploads/',$featured_small->media_details->sizes->large->source_url);      
    }
    $new_posts[$i]['featured_thumbnail'] = str_replace('/content/uploads/', $request->getUri()->getBaseUrl() . '/assets/dist/img/uploads/',$featured_small->media_details->sizes->medium->source_url);
    $i++;
  }

  //Save the WP content to a data.json file.
  file_put_contents('assets/dist/data/posts.json', json_encode($new_posts));
});

$app->get('/blog/[{path:.*}]', function($request, $response, $path = null) {
  $posts = json_decode(file_get_contents('assets/dist/data/posts.json'));
  $post_count = count($posts);
  $current_post = '';
  if( $path != null ) {
    $i = 0;
    foreach( $posts as $post ) {
      if( $post->slug == $path['path'] ) { 
        $current_post = $post;
        // break;
      } else {
        $otherPosts[$i] = $post;
        $i++;
      }
    }
    $page_title = $current_post->title;
    $page_description = strip_tags($current_post->excerpt);
    $page_slug = $current_post->slug;
    $site_image = $current_post->featured_image;
  } else {
    $page_title = 'Sometimes we write things...';
    $page_description = 'Sometimes we write things. Blog posts and musings from calder.io';
    $page_slug = '';
    $site_image = 'https://calder.io/assets/dist/img/calder-header.jpg';
  }
  $vars = [
    'page' => [
      'title' => 'calder.io :: ' . $page_title,
      'posts_title' => 'Sometimes we write things...',
      'description' => $page_description,
      'site_image'  => $site_image,
      'post'       => $current_post,
      'slug'    => '/blog/' . $page_slug,
      'posts'       => $otherPosts,
      'post_count'  =>$post_count,
      'social_media' => [
        'facebook' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg>',
          'link' => 'https://www.facebook.com/calderdotio'
        ],
        'twitter' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/></svg>',
          'link' => 'https://twitter.com/calderdotio'
        ],
        'github' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M256 70.7c-102.6 0-185.9 83.2-185.9 185.9 0 82.1 53.3 151.8 127.1 176.4 9.3 1.7 12.3-4 12.3-8.9V389.4c-51.7 11.3-62.5-21.9-62.5-21.9 -8.4-21.5-20.6-27.2-20.6-27.2 -16.9-11.5 1.3-11.3 1.3-11.3 18.7 1.3 28.5 19.2 28.5 19.2 16.6 28.4 43.5 20.2 54.1 15.4 1.7-12 6.5-20.2 11.8-24.9 -41.3-4.7-84.7-20.6-84.7-91.9 0-20.3 7.3-36.9 19.2-49.9 -1.9-4.7-8.3-23.6 1.8-49.2 0 0 15.6-5 51.1 19.1 14.8-4.1 30.7-6.2 46.5-6.3 15.8 0.1 31.7 2.1 46.6 6.3 35.5-24 51.1-19.1 51.1-19.1 10.1 25.6 3.8 44.5 1.8 49.2 11.9 13 19.1 29.6 19.1 49.9 0 71.4-43.5 87.1-84.9 91.7 6.7 5.8 12.8 17.1 12.8 34.4 0 24.9 0 44.9 0 51 0 4.9 3 10.7 12.4 8.9 73.8-24.6 127-94.3 127-176.4C441.9 153.9 358.6 70.7 256 70.7z"/></svg>',
          'link' => 'https://github.com/calderdotio'
        ],
        'stackoverflow' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M294.8 361.2l-122 0.1 0-26 122-0.1L294.8 361.2zM377.2 213.7L356.4 93.5l-25.7 4.5 20.9 120.2L377.2 213.7zM297.8 301.8l-121.4-11.2 -2.4 25.9 121.4 11.2L297.8 301.8zM305.8 267.8l-117.8-31.7 -6.8 25.2 117.8 31.7L305.8 267.8zM321.2 238l-105-62 -13.2 22.4 105 62L321.2 238zM346.9 219.7l-68.7-100.8 -21.5 14.7 68.7 100.8L346.9 219.7zM315.5 275.5v106.5H155.6V275.5h-20.8v126.9h201.5V275.5H315.5z"/></svg>',
          'link' => 'http://stackoverflow.com/users/1726511/rick-calder'
        ],
        'linkedin' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/></svg>',
          'link' => 'https://www.linkedin.com/company/calder.io'
        ]
      ]
    ],
  ]; 
  if( $path != null ) {
    return $this->view->render($response, 'blog.twig', $vars);
  } else {
    return $this->view->render($response, 'posts.twig', $vars);
  }
});


$app->get('/blog', function($request, $response, $path = null) {
  $posts = json_decode(file_get_contents('assets/dist/data/posts.json'));
  $current_post = '';
  if( $path != null ) {
    foreach( $posts as $post ) {
      if( $post->slug == $path['path'] ) { 
        $current_post = $post;
        break;
      }
    }
    $page_title = $current_post->title;
    $page_description = strip_tags($current_post->excerpt);
    $page_slug = $current_post->slug;
    $site_image = $current_post->featured_image;
  } else {
    $page_title = 'Sometimes we write things...';
    $page_description = 'Sometimes we write things. Blog posts and musings from calder.io';
    $page_slug = '';
    $site_image = 'https://calder.io/assets/dist/img/calder-header.jpg';
  }
  $vars = [
    'page' => [
      'title' => 'calder.io :: ' . $page_title,
      'posts_title' => 'Sometimes we write things...',
      'description' => $page_description,
      'site_image'  => $site_image,
      'post'       => $current_post,
      'slug'    => '/blog/' . $page_slug,
      'posts'       => $posts,
      'social_media' => [
        'facebook' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"/></svg>',
          'link' => 'https://www.facebook.com/calderdotio'
        ],
        'twitter' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"/></svg>',
          'link' => 'https://twitter.com/calderdotio'
        ],
        'github' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M256 70.7c-102.6 0-185.9 83.2-185.9 185.9 0 82.1 53.3 151.8 127.1 176.4 9.3 1.7 12.3-4 12.3-8.9V389.4c-51.7 11.3-62.5-21.9-62.5-21.9 -8.4-21.5-20.6-27.2-20.6-27.2 -16.9-11.5 1.3-11.3 1.3-11.3 18.7 1.3 28.5 19.2 28.5 19.2 16.6 28.4 43.5 20.2 54.1 15.4 1.7-12 6.5-20.2 11.8-24.9 -41.3-4.7-84.7-20.6-84.7-91.9 0-20.3 7.3-36.9 19.2-49.9 -1.9-4.7-8.3-23.6 1.8-49.2 0 0 15.6-5 51.1 19.1 14.8-4.1 30.7-6.2 46.5-6.3 15.8 0.1 31.7 2.1 46.6 6.3 35.5-24 51.1-19.1 51.1-19.1 10.1 25.6 3.8 44.5 1.8 49.2 11.9 13 19.1 29.6 19.1 49.9 0 71.4-43.5 87.1-84.9 91.7 6.7 5.8 12.8 17.1 12.8 34.4 0 24.9 0 44.9 0 51 0 4.9 3 10.7 12.4 8.9 73.8-24.6 127-94.3 127-176.4C441.9 153.9 358.6 70.7 256 70.7z"/></svg>',
          'link' => 'https://github.com/calderdotio'
        ],
        'stackoverflow' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M294.8 361.2l-122 0.1 0-26 122-0.1L294.8 361.2zM377.2 213.7L356.4 93.5l-25.7 4.5 20.9 120.2L377.2 213.7zM297.8 301.8l-121.4-11.2 -2.4 25.9 121.4 11.2L297.8 301.8zM305.8 267.8l-117.8-31.7 -6.8 25.2 117.8 31.7L305.8 267.8zM321.2 238l-105-62 -13.2 22.4 105 62L321.2 238zM346.9 219.7l-68.7-100.8 -21.5 14.7 68.7 100.8L346.9 219.7zM315.5 275.5v106.5H155.6V275.5h-20.8v126.9h201.5V275.5H315.5z"/></svg>',
          'link' => 'http://stackoverflow.com/users/1726511/rick-calder'
        ],
        'linkedin' => [
          'icon' => '<svg viewBox="0 0 512 512"><path d="M186.4 142.4c0 19-15.3 34.5-34.2 34.5 -18.9 0-34.2-15.4-34.2-34.5 0-19 15.3-34.5 34.2-34.5C171.1 107.9 186.4 123.4 186.4 142.4zM181.4 201.3h-57.8V388.1h57.8V201.3zM273.8 201.3h-55.4V388.1h55.4c0 0 0-69.3 0-98 0-26.3 12.1-41.9 35.2-41.9 21.3 0 31.5 15 31.5 41.9 0 26.9 0 98 0 98h57.5c0 0 0-68.2 0-118.3 0-50-28.3-74.2-68-74.2 -39.6 0-56.3 30.9-56.3 30.9v-25.2H273.8z"/></svg>',
          'link' => 'https://www.linkedin.com/company/calder.io'
        ]
      ]
    ],
  ]; 
  if( $path != null ) {
    return $this->view->render($response, 'blog.twig', $vars);
  } else {
    return $this->view->render($response, 'posts.twig', $vars);
  }
});