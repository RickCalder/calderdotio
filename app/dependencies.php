<?php

// Get the container
$container = $app->getContainer();

// Twig view dependency

$container['view'] = function ($c) {
  $cf = $c->get('settings')['view'];
  $view = new \Slim\Views\Twig($cf['path'], $cf['twig']);
  
  // Instantiate and add Slim specific extension
  $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
  $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));

  return $view;
};

