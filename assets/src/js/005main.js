// "use strict";
var $target;
(function($) {
  var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };

  //enable Bootstrap popovers
  $('[data-toggle="popover"]').popover().on("click",function(e){
    e.preventDefault();
  }); 

  //Function to keep containers equal height.
  var equalheight = function(container){
    var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = new Array(),
    $el,
    topPosition = 0;
    $(container).each(function() {
      $el = $(this);
      $($el).height("auto")
      topPosition = $el.position().top;

      if (currentRowStart != topPosition) {
        for (var currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }
        rowDivs.length = 0; // empty the array
        currentRowStart = topPostion;
        currentTallest = $el.height();
        rowDivs.push($el);
      } else {
        rowDivs.push($el);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
   });
  }

  //Show the back to top button
  $(document).on("scroll", debounce( function() {
    menuActive();
    windowTop = $(window).scrollTop();
    if( windowTop > 200 ) {
      $("#back-to-top").velocity({
        bottom: "0"
      });
    } else {
      $("#back-to-top").velocity({
        bottom: "-110px"
      });
    }

  }, 50));

  //Scroll to anchors
  $(".scroll-link").on("click", function(e) {
    e.preventDefault();
    $target = "#" + $(this).data("target");
    if( window.location.pathname !== "/" ) {
      window.location.href = "/" + $target;
    } else {
      $("html, body").animate({
        scrollTop: $($target).offset().top
      }, 500);
    }
  });

  $("#back-to-top").on("click", function() {
    $("html, body").velocity("scroll",{
      offset: 0
    });
  });


  // add the animation to the modal
  $(".modal").each(function(index) {
    $(this).on('show.bs.modal', function(e) {
      $('.modal-dialog').velocity('transition.expandIn');
    });
  });

  $(window).on('load', function(){
    if($(window).width() > 990) {
      equalheight(".package");
      equalheight(".blog-inner-container");
    }
  });

  $(window).resize(function(){
    if($(window).width() > 990) {
      debounce(equalheight(".package"));
    }
  });

  function menuActive(event){
    var scrollPos = $(document).scrollTop();
    $('.nav-links a').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));
      if(location.pathname === "/") {
        if (refElement.position().top <= scrollPos + 30 && refElement.position().top + refElement.height() > scrollPos) {
          $('#navul li a').removeClass("active");
          currLink.addClass("active");
        } else {
          currLink.removeClass("active");
        }
      }
    });
  }

  $("#contact-submit").on( "click", function(e) {
    e.preventDefault();
    $(".status-div").slideUp();
    if($("#last_name").val()!==""){
      $(".status-div").html("<p>Oh no! Something went wrong, you can try again or send us an email directly to <a href='mailto:info@calder.io'>info@calder.io</a>.</p>").addClass('error-div').removeClass('success-div').slideDown();
      return;
    };
    var errors = [];
    if($("#full_name").val() === "") {
      errors.push('Name is a required field');
    }
    if($("#email").val() === "") {
      errors.push('Email is a required field');
    }
    if(errors.length >0) {
      var html = "<p>Please fix the following errors and try again</p><ul>";
      for(var i=0; i<errors.length; i++) {
        html += "<li>" + errors[i] + "</li>";
      }
      html +="</ul>";
      $(".status-div").html(html).addClass('error-div').removeClass('success-div').slideDown();
      return;

    }
    var url = base_url + "/";
    $.ajax({
      type: "POST",
      url: url,
      data: $("#contact-form").serialize(), 
      dataType: "json",
      success: function(data) {
        if(data.status == 'error'){
          $(".status-div").html("<p>Oh no! Something went wrong, you can try again or send us an email directly to <a href='mailto:info@calder.io'>info@calder.io</a>.</p>").addClass('error-div').removeClass('success-div').slideDown();
        } else {
          $(".status-div").html("<p>Your request has been sent, we'll be in contact soon! Thank you for your interest </p>").removeClass('error-div').addClass('success-div').slideDown();
          }
        }
    });
  });

})(jQuery);


